import { ApiRequest } from "../request";

let baseUrl: string = global.appConfig.baseUrl;

export class ArticleCommentController {
    async postArticleComment(accessToken:string, articleComment) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`article_comment`)
            .bearerToken(accessToken)
            .body(articleComment)
            .send();
        return response;
    }

    async getArticleComments(accessToken:string, articleId:string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url(`article_comment/of/${articleId}?size=200`)
            .bearerToken(accessToken)
            .send();
        return response;
    }

}
