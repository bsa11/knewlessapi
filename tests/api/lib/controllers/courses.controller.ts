import { ApiRequest } from "../request";

let baseUrl: string = global.appConfig.baseUrl;

export class CoursesController {
    async getAllCourses() {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url(`course/all`) 
            .send();
        return response;
    }

    async getPopularCourses() {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url(`course/popular`)
            .send();
        return response;
    }

    async getAllCourseInfoById(id: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url(`course/${id}/info`) 
            .send();
        return response;
    }

    async getRecommendedCourses(accessToken:string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url(`course/recommended`)
            .bearerToken(accessToken) 
            .send();
        return response;
    }

    async getCourseInfoById(accessToken:string, courseId) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url(`course/${courseId}/info`)
            .bearerToken(accessToken) 
            .send();
        return response;
    }
}
