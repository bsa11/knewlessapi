import { ApiRequest } from "../request";

let baseUrl: string = global.appConfig.baseUrl;

export class ArticleController {
    async postArticle(accessToken:string, article) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`article`)
            .bearerToken(accessToken)
            .body(article)
            .send();
        return response;
    }

    async getArticles(accessToken:string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url(`article/author`)
            .bearerToken(accessToken)
            .send();
        return response;
    }

    async getArticle(accessToken:string, articleId:string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url(`article/${articleId}`)
            .bearerToken(accessToken)
            .send();
        return response;
    }

}
