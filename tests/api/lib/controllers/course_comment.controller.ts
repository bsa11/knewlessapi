import { ApiRequest } from "../request";

let baseUrl: string = global.appConfig.baseUrl;

export class CourseCommentController {
    async postCourseComment(accessToken:string, courseComment) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`course_comment`)
            .bearerToken(accessToken)
            .body(courseComment)
            .send();
        return response;
    }

    async getCourseComments(accessToken:string, courseId:string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url(`course_comment/of/${courseId}?size=200`)
            .bearerToken(accessToken)
            .send();
        return response;
    }

}
