import { ApiRequest } from "../request";

let baseUrl: string = global.appConfig.baseUrl;

export class StudentController {
    async getInfo(accessToken:string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url(`student/info`)
            .bearerToken(accessToken)
            .send();
        return response;
    }

    async getGoal(accessToken:string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url(`student/goal`)
            .bearerToken(accessToken)
            .send();
        return response;
    }

}
