import { ApiRequest } from "../request";

let baseUrl: string = global.appConfig.baseUrl;

export class AuthController {
    async authenticateUser(email:string, password:string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`auth/login`)
            .body({
                "email": `${email}`,
                "password": `${password}`
            }) 
            .send();
        return response;
    }

}
