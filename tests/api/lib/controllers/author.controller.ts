import { ApiRequest } from "../request";

let baseUrl: string = global.appConfig.baseUrl;

export class AuthorController {
    async getAuthor(accessToken:string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url(`author/`)
            .bearerToken(accessToken)
            .send();
        return response;
    }

    async getAuthorOverview(accessToken:string, userId:string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url(`author/overview/${userId}`)
            .bearerToken(accessToken)
            .send();
        return response;
    }

    async postAuthor(accessToken:string, authorInfo) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`author/`)
            .bearerToken(accessToken)
            .body(authorInfo)
            .send();
        return response;
    }

}
