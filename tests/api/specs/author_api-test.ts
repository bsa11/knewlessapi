import { expect } from "chai";
import { 
    checkResponseBodyJSONSchema, 
    checkResponseBodyMessage, 
    checkResponseBodyStatus, 
    checkResponseTime, 
    checkStatusCode 
} from "../../helpers/functionsForChecking.helper";
import { ArticleController } from "../lib/controllers/article.controller";
import { ArticleCommentController } from "../lib/controllers/article_comment.controller";
import { AuthController } from "../lib/controllers/auth.controller";
import { AuthorController } from "../lib/controllers/author.controller";
import { UserController } from "../lib/controllers/user.controller";
const articleComment = new ArticleCommentController();
const article = new ArticleController();
const auth = new AuthController();
const author = new AuthorController();
const user = new UserController();
const schemas = require('./data/schemas_testData.json');
var chai = require('chai');
chai.use(require('chai-json-schema'));


describe("Author can create an article and post a comment on the article", () => {
    let accessToken: string;
    let authorInfo;
    let authorId: string;
    let newFirstName = "Test";
    let newArticle ={
        "image": "https://knewless.tk/assets/images/1bd6ae23-a0e6-4f3a-b966-b20b5f0871ae.jpg",
        "name": "New Article",
        "text": "<p>Some content</p>\n"
    }
    let CommentsDataSet = [
        { "text": "Some comment", "articleId": "" },
        { "text": "Another comment", "articleId": "" },
        { "text": "Some another comment", "articleId": "" },
        { "text": "Comment", "articleId": "" },
        { "text": "Fifth comment", "articleId": "" },
        { "text": "Six comment", "articleId": "" },
    ];
    let articleId: string;

    before(`should get access token and author info`, async () => {
        // runs once before the first test in this block
        let response = await auth.authenticateUser(global.appConfig.users.Yurii_admin.email, global.appConfig.users.Yurii_admin.password);
        checkStatusCode(response, 200);
        checkResponseTime(response,5000);
        checkResponseBodyJSONSchema(response,schemas.schema_login);
        accessToken = response.body.accessToken;

        response = await author.getAuthor(accessToken);
        checkStatusCode(response, 200);
        checkResponseTime(response,5000);
        checkResponseBodyJSONSchema(response,schemas.schema_author_info);
        authorInfo = response.body;
        authorId = authorInfo.id;
    });

    it(`Change author info`, async () => {
        authorInfo.firstName = newFirstName;
        delete authorInfo.userId;
        authorInfo.uploadImage = null;
        let response = await author.postAuthor(accessToken,authorInfo);

        checkStatusCode(response, 200);
        checkResponseTime(response,5000);
        checkResponseBodyMessage(response,"Success. Your profile has been updated.");

    });

    it(`Get user data`, async () => {
        let response = await user.getCurrentUser(accessToken);

        checkStatusCode(response, 200);
        checkResponseTime(response,5000);
        checkResponseBodyJSONSchema(response,schemas.schema_me);

    });

    it(`Get author overview`, async () => {
        let response = await author.getAuthorOverview(accessToken,authorId);

        checkStatusCode(response, 200);
        checkResponseTime(response,5000);
        checkResponseBodyJSONSchema(response,schemas.schema_author_overview);

    });

    it(`Create new article`, async () => {
        let response = await article.postArticle(accessToken,newArticle);

        checkStatusCode(response, 200);
        checkResponseTime(response,5000);
        checkResponseBodyJSONSchema(response,schemas.schema_post_article);
        
        articleId = response.body.id;

    });

    it(`Get author articles`, async () => {
        let response = await article.getArticles(accessToken);

        checkStatusCode(response, 200);
        checkResponseTime(response,5000);
        checkResponseBodyJSONSchema(response,schemas.schema_articles);

    });

    it(`Get article`, async () => {
        let response = await article.getArticle(accessToken,articleId);

        checkStatusCode(response, 200);
        checkResponseTime(response,5000);
        checkResponseBodyJSONSchema(response,schemas.schema_article_by_id);
        expect(response.body.name).to.be.equal(newArticle.name);
        expect(response.body.image).to.be.equal(newArticle.image);
        expect(response.body.text).to.be.equal(newArticle.text);
        expect(response.body.author.id).to.be.equal(authorId);
    });

    CommentsDataSet.forEach((newComment) => {
        it(`Post comment on the course`, async () => {
            newComment.articleId = articleId;
            let response = await articleComment.postArticleComment(accessToken,newComment);
    
            checkStatusCode(response, 200);
            checkResponseTime(response,5000);
            checkResponseBodyJSONSchema(response,schemas.schema_article_post_comment);
            expect(response.body.articleId).to.be.equal(articleId);
            expect(response.body.text).to.be.equal(newComment.text);
        });
    })

    it(`Get comments on the article`, async () => {
        let response = await articleComment.getArticleComments(accessToken,articleId);

        checkStatusCode(response, 200);
        checkResponseTime(response,5000);
        checkResponseBodyJSONSchema(response,schemas.schema_article_comments);
    });

    it(`Post comment on the article without authorization`, async () => {
        CommentsDataSet[0].articleId = articleId;
        let response = await articleComment.postArticleComment("",CommentsDataSet[0]);

        checkStatusCode(response, 401);
        checkResponseTime(response,5000);
        expect(response.body.error).to.be.equal("Unauthorized");
        expect(response.body.path).to.be.equal("/article_comment");
    });

    it(`Post comment on the article without article ID`, async () => {
        CommentsDataSet[0].articleId = "";
        let response = await articleComment.postArticleComment(accessToken,CommentsDataSet[0]);

        checkStatusCode(response, 400);
        checkResponseTime(response,5000);
    });

});
