import { expect } from "chai";
import { 
    checkResponseBodyJSONSchema, 
    checkResponseBodyStatus, 
    checkResponseTime,
    checkStatusCode 
} from "../../helpers/functionsForChecking.helper";
import { AuthController } from "../lib/controllers/auth.controller";
import { CoursesController } from "../lib/controllers/courses.controller";
import { CourseCommentController } from "../lib/controllers/course_comment.controller";
import { StudentController } from "../lib/controllers/student.controller";
import { UserController } from "../lib/controllers/user.controller";
const student = new StudentController();
const auth = new AuthController();
const user = new UserController();
const course = new CoursesController();
const courseComment = new CourseCommentController();
const schemas = require('./data/schemas_testData.json');
var chai = require('chai');
chai.use(require('chai-json-schema'));


describe("Student can view a course and post a comment on the course", () => {
    let accessToken: string;
    let userId: string;
    let courseId: string;
    let CommentsDataSet = [
        { "text": "Some comment", "courseId": "" },
        { "text": "Another comment", "courseId": "" },
        { "text": "Some another comment", "courseId": "" },
        { "text": "Comment", "courseId": "" },
        { "text": "Fifth comment", "courseId": "" },
        { "text": "Six comment", "courseId": "" },
    ];

    before(`should get access token and userId`, async () => {
        // runs once before the first test in this block
        let response = await auth.authenticateUser(global.appConfig.users.Yurii_student.email, global.appConfig.users.Yurii_student.password);
        checkStatusCode(response, 200);
        checkResponseTime(response,5000);
        checkResponseBodyJSONSchema(response,schemas.schema_login);
        accessToken = response.body.accessToken;

        response = await user.getCurrentUser(accessToken);
        checkStatusCode(response, 200);
        checkResponseTime(response,5000);
        checkResponseBodyJSONSchema(response,schemas.schema_me);
        userId = response.body.id;
    });

    it(`Get student info`, async () => {
        let response = await student.getInfo(accessToken);

        checkStatusCode(response,200);
        checkResponseTime(response,5000);
        checkResponseBodyJSONSchema(response,schemas.schema_student_info);

    });

    it(`Get student goal`, async () => {
        let response = await student.getGoal(accessToken);

        checkStatusCode(response,200);
        checkResponseTime(response,5000);
        checkResponseBodyJSONSchema(response,schemas.schema_student_goal);
 
    });

    it(`Get recommended courses`, async () => {
        let response = await course.getRecommendedCourses(accessToken);

        checkStatusCode(response,200);
        checkResponseTime(response,5000);
        checkResponseBodyJSONSchema(response,schemas.schema_recommended_courses);
     
        courseId = response.body[0].id;
    });

    it(`View course`, async () => {
        let response = await course.getCourseInfoById(accessToken,courseId);

        checkStatusCode(response,200);
        checkResponseTime(response,5000);
        checkResponseBodyJSONSchema(response,schemas.schema_course_info);
  
    });

    CommentsDataSet.forEach((newComment) => {
        it(`Post comment on the course`, async () => {
            newComment.courseId = courseId;
            let response = await courseComment.postCourseComment(accessToken,newComment);

            checkStatusCode(response,200);
            checkResponseTime(response,5000);
            checkResponseBodyJSONSchema(response,schemas.schema_course_post_comment);
            expect(response.body.courseId).to.be.equal(courseId);
            expect(response.body.text).to.be.equal(newComment.text);
       });
    })

    it(`Get comments on the course`, async () => {
        let response = await courseComment.getCourseComments(accessToken,courseId);

        checkStatusCode(response,200);
        checkResponseTime(response,5000);
        checkResponseBodyJSONSchema(response,schemas.schema_course_comments);
    });

    it(`Post comment on the course without authorization`, async () => {
        CommentsDataSet[0].courseId = courseId;
        let response = await courseComment.postCourseComment("",CommentsDataSet[0]);

        checkStatusCode(response,401);
        checkResponseTime(response,5000);
        expect(response.body.error).to.be.equal("Unauthorized");
        expect(response.body.path).to.be.equal("/course_comment");
    });

    it(`Post comment on the course without course ID`, async () => {
        CommentsDataSet[0].courseId = "";
        let response = await courseComment.postCourseComment(accessToken,CommentsDataSet[0]);

        checkStatusCode(response,400);
        checkResponseTime(response,5000);
    });
});
