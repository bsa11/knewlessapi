global.appConfig = {
    envName: 'DEV Environment',
    baseUrl: 'https://knewless.tk/api/',
    swaggerUrl: 'https://knewless.tk/api/swagger-ui/index.html',

    users: {
        Yurii_admin: {
            email: 'radkov91@gmail.com',
            password: 'qwerty12345',
        },
        Yurii_student: {
            email: 'yradkov91@gmail.com',
            password: 'qwerty12345',
        },
    }
};
